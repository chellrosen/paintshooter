// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

    #include "FooLensDistortionBlueprintLibrary.h"

//Chell Rosen Final Project Part A
//changed the name and refernces for the lens distortion class for the Foo plugin - no other changes made
//changes for how this shader is used where made in blueprints


    UFooLensDistortionBlueprintLibrary::UFooLensDistortionBlueprintLibrary(const FObjectInitializer& ObjectInitializer)
        : Super(ObjectInitializer)
    { }

    // static
    void UFooLensDistortionBlueprintLibrary::GetUndistortOverscanFactor(
        const FFooCameraModel& CameraModel,
        float DistortedHorizontalFOV,
        float DistortedAspectRatio,
        float& UndistortOverscanFactor)
    {
        UndistortOverscanFactor = CameraModel.GetUndistortOverscanFactor(DistortedHorizontalFOV, DistortedAspectRatio);
    }

    // static
    void UFooLensDistortionBlueprintLibrary::DrawUVDisplacementToRenderTarget(
        const UObject* WorldContextObject,
        const FFooCameraModel& CameraModel,
        float DistortedHorizontalFOV,
        float DistortedAspectRatio,
        float UndistortOverscanFactor,
        class UTextureRenderTarget2D* OutputRenderTarget,
        float OutputMultiply,
        float OutputAdd)
    {
        CameraModel.DrawUVDisplacementToRenderTarget(
            WorldContextObject->GetWorld(),
            DistortedHorizontalFOV, DistortedAspectRatio,
            UndistortOverscanFactor, OutputRenderTarget,
            OutputMultiply, OutputAdd);
    }