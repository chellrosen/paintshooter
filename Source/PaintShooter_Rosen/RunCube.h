// Fill out your copyright notice in the Description page of Project Settings.

// Chell Rosen Final Project A
//runtime cube the size has been edited from the tutorial runtime cube - changed the default size

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RuntimeMeshComponent.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "RunCube.generated.h"

UCLASS()
class PAINTSHOOTER_ROSEN_API ARunCube : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ARunCube();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	virtual void PostActorCreated() override;
	virtual void PostLoad() override;
	virtual void GenerateBoxMesh();
	virtual void CreateBoxMesh(FVector BoxRadius, TArray<FVector>&Vertices, TArray<int32>&Triangles,
		TArray<FVector>&Normals, TArray<FVector2D> &UVs, TArray<FRuntimeMeshTangent>&Tangents, TArray<FColor>&Colors);
private:
	UPROPERTY(VisibleAnywhere)
		URuntimeMeshComponent * mesh;

	UPROPERTY()
		UMaterialInterface * m_dynamicMaterial;


};
