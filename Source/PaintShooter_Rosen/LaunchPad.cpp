// Fill out your copyright notice in the Description page of Project Settings.

#include "LaunchPad.h"
#include "PaintShooter_RosenCharacter.h"

// Sets default values
ALaunchPad::ALaunchPad()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	Box1 = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));

	Plane = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PlaneMesh"));
	Plane2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PlaneMesh2"));
	SetRootComponent(Plane);
	Plane->SetWorldScale3D(FVector(250, 250, 1));
	Box1->InitBoxExtent(FVector(.5, .5, 25));
	Box1->SetupAttachment(Plane);
	Box1->SetRelativeLocation(FVector(0, 0, 25));
	Plane2->SetupAttachment(Plane);
	Plane2->SetRelativeRotation(FRotator(0, 0, 180));

	static ConstructorHelpers::FObjectFinder<UStaticMesh>asset(TEXT("/Engine/ArtTools/RenderToTexture/Meshes/S_1_Unit_Plane.S_1_Unit_Plane"));
	Plane->SetStaticMesh(asset.Object);
	Plane2->SetStaticMesh(asset.Object);
	

	//load the material the matrial for the triangle has me
	static ConstructorHelpers::FObjectFinder<UMaterialInterface>asset1(TEXT("/Game/FirstPersonCPP/Blueprints/LaunchPad"));
	ShaderMaterial = asset1.Object;

	//Apply the shader to the mesh
	Plane->SetMaterial(0, ShaderMaterial);
	Plane2->SetMaterial(0, ShaderMaterial);

	Box1->OnComponentBeginOverlap.AddDynamic(this, &ALaunchPad::OnOverlapBegin);
	Box1->bGenerateOverlapEvents = true;
}

// Called when the game starts or when spawned
void ALaunchPad::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALaunchPad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALaunchPad::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != NULL)
	{
		APaintShooter_RosenCharacter* OverlapC = Cast<APaintShooter_RosenCharacter>(OtherActor);

		OverlapC->LaunchCharacter(LaunchVelocity, true, true);
		
	}

}