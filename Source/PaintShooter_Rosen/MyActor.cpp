// Fill out your copyright notice in the Description page of Project Settings.

#include "MyActor.h"


//create triangles based on the vertices's added

// Sets default values
//this is the my actor tutorial its not in the project as it is a 2D object

AMyActor::AMyActor()
{
	//create the default sub-object mesh
	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	//make the root component the mesh
	RootComponent = mesh;

}

void AMyActor::PostActorCreated()
{
	Super::PostActorCreated();//override PostActorCreated
	CreateSquare();

}

void AMyActor::PostLoad()
{
	Super::PostLoad();
	CreateSquare();
}


void AMyActor::CreateSquare()
{
	TArray<FVector> Vertices;
	TArray<int32> Triagles;
	TArray<FVector> Normals;
	TArray<FLinearColor>Colors;
	Vertices.Add(FVector(0.f, 0.f, 0.f));
	Vertices.Add(FVector(0.f, 100.f, 0.f));
	Vertices.Add(FVector(0.f, 0.f, 100.f));
	Vertices.Add(FVector(0.f, 100.f, 100.f));
	Triagles.Add(0);
	Triagles.Add(1);
	Triagles.Add(2);
	Triagles.Add(3);
	Triagles.Add(2);
	Triagles.Add(1);

	for (int32 i = 0; i < Vertices.Num(); i++)
	{
		Normals.Add(FVector(0.f, 0.f, 1.f));
		Colors.Add(FLinearColor::Red);
	}

	//optionalarrays
	TArray<FVector2D>UV0;
	TArray<FProcMeshTangent> Tangents;

	mesh->CreateMeshSection_LinearColor(0, Vertices, Triagles, Normals, UV0, Colors, Tangents, true);

}


// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

