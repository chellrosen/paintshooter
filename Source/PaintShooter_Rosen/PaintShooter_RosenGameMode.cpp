// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "PaintShooter_RosenGameMode.h"
#include "PaintShooter_RosenHUD.h"
#include "PaintShooter_RosenCharacter.h"
#include "UObject/ConstructorHelpers.h"

APaintShooter_RosenGameMode::APaintShooter_RosenGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = APaintShooter_RosenHUD::StaticClass();
}
