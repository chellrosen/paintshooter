// Fill out your copyright notice in the Description page of Project Settings.

#include "BoxTriggerA.h"
#include "PaintShooter_RosenCharacter.h"

//Chell Rosen Final Project A
//changes the color of the projectile being fired and the splat color each time the player passed through the square 
//box trigger has both the trigger component and a plane that applies the shader using c++
//the plane is the root component and the box trigger is attached to that
//the shader is applied to the plane here but controlled in the blueprints


// Sets default values
ABoxTriggerA::ABoxTriggerA()
{
	Box1 = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	
	plane = CreateDefaultSubobject<UStaticMeshComponent> (TEXT("PlaneMesh"));
	SetRootComponent(plane);
	plane->SetWorldScale3D(FVector(250, 250, 1));
	Box1->InitBoxExtent(FVector(.5, .5, 25));
	Box1->SetupAttachment(plane);
	Box1->SetRelativeLocation(FVector(0, 0, 25));

	

	static ConstructorHelpers::FObjectFinder<UStaticMesh>asset(TEXT("/Engine/ArtTools/RenderToTexture/Meshes/S_1_Unit_Plane.S_1_Unit_Plane"));
	plane->SetStaticMesh(asset.Object);
	


	//load the material the material for the triangle has me
	static ConstructorHelpers::FObjectFinder<UMaterialInterface>asset1(TEXT("/Game/Shaders/MAT_RT.MAT_RT"));
	shaderMaterial = asset1.Object;

	//Apply the shader to the mesh
	plane->SetMaterial(0, shaderMaterial);

	Box1->OnComponentBeginOverlap.AddDynamic(this, &ABoxTriggerA::OnOverlapBegin);
	Box1->bGenerateOverlapEvents = true;


}

// Called when the game starts or when spawned
void ABoxTriggerA::BeginPlay()
{

	Super::BeginPlay();
	
}


void ABoxTriggerA::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != NULL)
	{
		APaintShooter_RosenCharacter* OverlapC = Cast<APaintShooter_RosenCharacter>(OtherActor);

		if (OverlapC)
		{
			if (isBlueProjectile)
			{
				OverlapC->SetProjectilePurple();
				isBlueProjectile = false;
			}
			else
			{
				OverlapC->SetProjectileBlue();
				isBlueProjectile = true;
			}
		}
	}

}