// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PaintShooter_RosenGameMode.generated.h"

UCLASS(minimalapi)
class APaintShooter_RosenGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APaintShooter_RosenGameMode();
};



