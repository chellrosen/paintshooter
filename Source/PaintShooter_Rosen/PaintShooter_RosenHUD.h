// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "PaintShooter_RosenHUD.generated.h"

UCLASS()
class APaintShooter_RosenHUD : public AHUD
{
	GENERATED_BODY()

public:
	APaintShooter_RosenHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

