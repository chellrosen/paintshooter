// Fill out your copyright notice in the Description page of Project Settings.


// Chell Rosen Final Project A
//created a pyramid mesh based off the cube tutorial where instead of generating 2 triangles for the side just one is generated
//those single triangles share one top point and thus they form a pyramid 
//


#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RuntimeMeshComponent.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "RunPyramid.generated.h"

UCLASS()
class PAINTSHOOTER_ROSEN_API ARunPyramid : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ARunPyramid();

protected:
	void OnActorHit();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:


	virtual void PostActorCreated() override; //called after the actor is created
	virtual void PostLoad() override; //called after load or start up
	virtual void GenerateBoxMesh(); //generates the arrays for the mesh 

	virtual void CreateBoxMesh(FVector BoxRadius, TArray<FVector>&Vertices, TArray<int32>&Triangles, TArray<FColor>&Colors); //creates the mesh
private:
	UPROPERTY(VisibleAnywhere)
		URuntimeMeshComponent * mesh;
	UPROPERTY()
		UMaterialInterface * m_dynamicMaterial;
};
