// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "GameFramework/Actor.h"
#include "LaunchPad.generated.h"


UCLASS()
class PAINTSHOOTER_ROSEN_API ALaunchPad : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALaunchPad();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		UBoxComponent* Box1;

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	FVector LaunchVelocity = FVector(0.0f, 0.0f, 875.0f);

private:

	UPROPERTY()
		UMaterialInterface* ShaderMaterial;
	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent * Plane;
	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent * Plane2;

};
