// Fill out your copyright notice in the Description page of Project Settings.

// Chell Rosen Final Project A
//created a pyramid mesh based off the cube tutorial where instead of generating 2 triangles for the side just one is generated


#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "Pyramid.generated.h"

UCLASS()
class PAINTSHOOTER_ROSEN_API APyramid : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APyramid();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
public:

	virtual void PostActorCreated() override; //called after the actor is created
	virtual void PostLoad() override;//called after load or start up
	virtual void GenerateBoxMesh(); //generates the arrays for the mesh 

	virtual void CreateBoxMesh(FVector BoxRadius, TArray<FVector>&Vertices, TArray<int32>&Triangles, TArray<FColor>&Colors);  //creates the mesh
private:
	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent * mesh;

	UPROPERTY()
		UMaterialInterface * m_dynamicMaterial;
};
