// Fill out your copyright notice in the Description page of Project Settings.
 
//Chell Rosen Final Project A
// .h file for box trigger that changes the projectile

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "BoxTriggerA.generated.h"


UCLASS()
class PAINTSHOOTER_ROSEN_API ABoxTriggerA : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABoxTriggerA();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	//boolean for the color of the projectile
	bool isBlueProjectile = true;


public:	
	
	UPROPERTY(EditAnywhere)
		UBoxComponent* Box1;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
private:
	
	UPROPERTY()
		UMaterialInterface* shaderMaterial;
	UPROPERTY(VisibleAnywhere) 
		UStaticMeshComponent * plane;
};
